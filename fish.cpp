///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file fish.cpp
/// @version 1.0
///
/// Exports data about all fish
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <iostream> 

#include "fish.hpp"

using namespace std;

namespace animalfarm {
	
void Fish::printInfo() {
	Animal::printInfo();
	cout << "   Scale Color = [" << colorName( scaleColor ) << "]" << endl;
	cout << "   Favorite Temprature = [" << favoriteTemp << "]" << endl;
}


const string Fish::speak() {
	return string( "Bubble bubble" );
}


} // namespace animalfarm
